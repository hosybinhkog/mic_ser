/**
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

/** @type {import('jest').Config} */
const config = {
  clearMocks: true,
  collectCoverage: true,
  coveragePathIgnorePatterns: ["/node_modules/"],
  coverageProvider: "v8",
  preset: "ts-jest",
  verbose: true,
  moduleDirectories: ["node_modules", "src"],
  coverageDirectory: "coverage",
};

module.exports = config;
