import { Product } from "./../../models/product.models";

export interface DtoInputProduct {
  id?: string | number;
  name?: string;
  description?: string;
  stock?: number;
  price?: number;
}

export interface ICatalogRepository {
  create(data: DtoInputProduct): Promise<Product | Partial<Product>>;
  update(data: DtoInputProduct): Promise<Product>;
  delete(id: any): any;
  find(limit: number, offset: number): Promise<Product[]>;
  findOne(id: number | string): Promise<Product | null>;
}
