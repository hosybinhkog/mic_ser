import dotenv from "dotenv";
dotenv.config();

import expressApp from "./expressApp";

const PORT = process.env.PORT || 3333;
export const startServer = async () => {
  expressApp.listen(PORT, () =>
    console.log("server listening on http://localhost:" + PORT)
  );

  process.on("uncaughtException", async (err) => {
    console.log(err);
    process.exit(1);
  });
};

startServer().then(() => console.log("startup"));
