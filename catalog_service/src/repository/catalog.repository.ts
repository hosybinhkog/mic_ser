import { ICatalogRepository } from "../interface/repository/catalog.interface";
import { Product } from "../models/product.models";
import { PrismaClient } from "@prisma/client";

export class CatalogRepository implements ICatalogRepository {
  _prisma: PrismaClient;
  constructor() {
    this._prisma = new PrismaClient();
  }
  async create(input: Product): Promise<Product> {
    return this._prisma.product.create({
      data: {
        name: input.name,
        description: input.description,
        price: input.price,
        stock: input.stock,
      },
    });
  }
  async update(data: Product): Promise<Product> {
    const id = parseInt(data.id as string);

    return this._prisma.product.update({
      where: { id },
      data: {
        name: data.name,
        description: data.description,
        price: data.price,
        stock: data.stock,
      },
    });
  }
  async delete(id: any) {
    const idProduct = parseInt(id);
    return this._prisma.product.delete({ where: { id: idProduct } });
  }
  async find(limit: number, offset: number): Promise<Product[]> {
    return this._prisma.product.findMany({ take: limit, skip: offset });
  }
  async findOne(id: number): Promise<Product | null> {
    const product = await this._prisma.product.findFirst({
      where: { id: id },
    });
    if (!product) throw new Error("Product not found");
    return product;
  }
}
