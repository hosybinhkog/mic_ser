import express, { Request, Response } from "express";
import catalogRouter from "./apis/catalog.routes";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true, limit: "50mb" }));

app.use(catalogRouter);

export default app;
