import { faker } from "@faker-js/faker";
import { Product } from "./../../models/product.models";
import { Factory } from "rosie";

export const ProductFactory = new Factory<Product>()
  .attr("name", faker.commerce.productName())
  .attr("description", faker.commerce.productDescription())
  .attr("price", faker.number.int({ max: 100, min: 10 }))
  .attr("stock", faker.number.int({ max: 100, min: 1 }));
