import { ProductFactory } from "./../../utils/fixtures/index";
import request from "supertest";
import express from "express";
import { faker } from "@faker-js/faker";

import catalogRouter, { catalogService } from "../../apis/catalog.routes";
import { describe } from "node:test";

const app = express();
app.use(express.json());

app.use(catalogRouter);

const mockRequest = (rest?: any) => {
  return {
    name: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    price: faker.number.int({ min: 10, max: 1000 }),
    stock: faker.number.int({ min: 10, max: 1000 }),
    ...rest,
  };
};

describe("CatalogRouter", () => {
  describe("POST /products", () => {
    test("should create product successfully", async () => {
      const reqBody = mockRequest();

      const product = ProductFactory.build();

      jest
        .spyOn(catalogService, "createProduct")
        .mockImplementationOnce(() => Promise.resolve(product));

      const response = await request(app)
        .post("/product")
        .set("Accept", "application/json")
        .send(reqBody);
      expect(response.status).toBe(201);
      expect(response.body.data).toEqual(product);
      expect(response.body.message).toBe("Hello apis");
    });

    test("should response with validate error", async () => {
      const reqBody = mockRequest();

      const response = await request(app)
        .post("/product")
        .set("Accept", "application/json")
        .send({ ...reqBody, name: "" });
      console.log(response.body);
      expect(response.status).toBe(400);
      expect(response.body).toEqual("name is required");
    });

    test("should response with internal error", async () => {
      const reqBody = mockRequest();

      jest
        .spyOn(catalogService, "createProduct")
        .mockImplementationOnce(() =>
          Promise.reject(new Error("server internal error"))
        );

      const response = await request(app)
        .post("/product")
        .set("Accept", "application/json")
        .send(reqBody);

      expect(response.status).toBe(500);
      expect(response.body).toMatch("server internal error");
    });
  });

  describe("PATCH product/:id", () => {
    test("should update product successfully", async () => {
      const product = ProductFactory.build();

      const requestBody = {
        name: product.name,
        description: product.description,
        stock: product.stock,
        price: product.price,
      };
      jest
        .spyOn(catalogService, "updateProduct")
        .mockImplementationOnce(() => Promise.resolve(product));

      const response = await request(app)
        .patch(`/product/${product.id}`)
        .set("Accept", "application/json")
        .send(requestBody);

      expect(response.status).toBe(200);
    });

    test("should update product validator", async () => {
      const product = ProductFactory.build();
      const requestBody = {
        name: "",
        description: product.description,
        stock: product.stock,
        price: -1,
      };
      const response = await request(app)
        .patch(`/product/${product.id}`)
        .set("Accept", "application/json")
        .send(requestBody);

      expect(response.status).toBe(400);
      expect(response.body).toEqual("Price min is 1");
    });

    test("should by update product server error internal", async () => {
      const reqBody = mockRequest();
      const product = ProductFactory.build();

      jest
        .spyOn(catalogService, "updateProduct")
        .mockImplementationOnce(() =>
          Promise.reject(new Error("server internal error"))
        );

      const response = await request(app)
        .patch(`/product/${product.id}`)
        .set("Accept", "application/json")
        .send(reqBody);

      expect(response.status).toBe(500);
      expect(response.body).toEqual("server internal error");
    });
  });

  describe("GET /product?limit=0&offset=0", () => {
    test("GET products successfully", async () => {
      const randomLimit = faker.number.int({ min: 10, max: 1000 });
      const products = ProductFactory.buildList(randomLimit);

      jest
        .spyOn(catalogService, "getProducts")
        .mockImplementationOnce(() => Promise.resolve(products));

      const response = await request(app)
        .get(`/product?limit=${randomLimit}&offset=0`)
        .set("Accept", "application/json");

      expect(response.status).toBe(200);
      expect(response.body.data).toMatchObject(products);
      expect(response.body.message).toMatch("get products successfully");
      expect(response.body.data.length).toBe(products.length);
    });

    test("GET products server internal error", async () => {
      const randomLimit = faker.number.int({ min: 10, max: 1000 });
      jest
        .spyOn(catalogService, "getProducts")
        .mockImplementationOnce(() =>
          Promise.reject(new Error("server internal error"))
        );

      const response = await request(app)
        .get(`/product?limit=${randomLimit}&offset=0`)
        .set("Accept", "application/json");

      expect(response.status).toBe(500);
      expect(response.body).toMatch("server internal error");
    });
  });

  describe("GEP product/:id", () => {
    test("get product successfully", async () => {
      const product = ProductFactory.build();

      jest
        .spyOn(catalogService, "getProduct")
        .mockImplementationOnce(() => Promise.resolve(product));

      const response = await request(app)
        .get(`/product/${product.id}`)
        .set("Accept", "application/json");

      expect(response.status).toBe(200);
      expect(response.body.message).toEqual("get product successfully");
      expect(response.body.data).toMatchObject(product);
    });

    test("get product error internal error", async () => {
      const product = ProductFactory.build();

      jest
        .spyOn(catalogService, "getProduct")
        .mockImplementationOnce(() =>
          Promise.reject(new Error("server internal error"))
        );

      const response = await request(app)
        .get(`/product/${product.id}`)
        .set("Accept", "application/json");

      expect(response.status).toBe(500);
      expect(response.body).toMatch("server internal error");
    });
  });

  describe("DELETE product/:id", () => {
    test("delete product/:id successfully", async () => {
      const product = ProductFactory.build();
      const id = product.id || 0;

      jest
        .spyOn(catalogService, "deleteProduct")
        .mockImplementationOnce(() => Promise.resolve({ id }));

      const response = await request(app)
        .delete(`/product/${product.id}`)
        .set("Accept", "application/json");

      expect(response.status).toBe(200);
      expect(response.body.data).toMatchObject({ id });
    });
  });
});
