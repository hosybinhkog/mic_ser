import {
  DtoInputProduct,
  ICatalogRepository,
} from "../../interface/repository/catalog.interface";
import { Product } from "../../models/product.models";
import { MockCatalogRepository } from "../../repository/mockCatalog.repository";
import { CatalogService } from "../../services/catalog.service";
import { faker } from "@faker-js/faker";
import { Factory } from "rosie";
import { v4 } from "uuid";

const productFactory = new Factory<Product>()
  .attr("id", v4())
  .attr("description", faker.commerce.productDescription())
  .attr("name", faker.commerce.productName())
  .attr("price", faker.number.int({ min: 10, max: 1000 }))
  .attr("stock", faker.number.int({ min: 10, max: 1000 }));

const mockerProduct: (rest?: any) => DtoInputProduct = (rest: any) => {
  return {
    name: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    stock: faker.number.int({ min: 0, max: 1000 }),
    price: faker.number.int({ min: 0, max: 1000 }),
    ...rest,
  };
};

describe("catalogService", () => {
  let repository: ICatalogRepository;
  beforeEach(() => {
    repository = new MockCatalogRepository();
  });
  afterEach(() => {
    repository = {} as MockCatalogRepository;
  });

  describe("createProduct", () => {
    test("should create product", async () => {
      const service = new CatalogService(repository);

      const requestBody: DtoInputProduct = mockerProduct();

      const result = await service.createProduct(requestBody);
      expect(result).toMatchObject({
        id: expect.any(Number),
        description: expect.any(String),
        price: expect.any(Number),
        stock: expect.any(Number),
        name: expect.any(String),
      });
    });

    test("shout throw error with product already exists", async () => {
      const service = new CatalogService(repository);

      const reqBody: DtoInputProduct = mockerProduct();

      jest
        .spyOn(repository, "create")
        .mockImplementationOnce(() => Promise.resolve({} as Product));

      await expect(service.createProduct(reqBody)).rejects.toThrow(
        "unable to create product"
      );
    });

    test("shout throw error with product already exists", async () => {
      const service = new CatalogService(repository);

      const reqBody: DtoInputProduct = mockerProduct();

      jest
        .spyOn(repository, "create")
        .mockImplementationOnce(() =>
          Promise.resolve(new Error("product already exists"))
        );

      await expect(service.createProduct(reqBody)).rejects.toThrow(
        "unable to create product"
      );
    });
  });

  describe("updateProduct", () => {
    test("should update product", async () => {
      const service = new CatalogService(repository);
      const reqBody = mockerProduct({
        id: faker.number.int({ min: 10, max: 100 }),
      });

      const result = await service.updateProduct(reqBody);
      expect(result).toMatchObject(reqBody);
    });

    // test("should update product throw error product not exits", async () => {
    //   const service = new CatalogService(repository);

    //   const id = faker.number.int({ min: 10, max: 100 });

    //   jest
    //     .spyOn(repository, "update")
    //     .mockImplementationOnce(() => Promise.reject("Product not found"));

    //   await expect(service.updateProduct({ id })).rejects.toThrow(
    //     "unable error"
    //   );
    // });
  });

  describe("getProducts", () => {
    test("should get products by offset and limit", async () => {
      const service = new CatalogService(repository);
      const randomLimit = faker.number.int({ min: 10, max: 50 });

      const products = productFactory.buildList(randomLimit);

      jest
        .spyOn(repository, "find")
        .mockImplementationOnce(() => Promise.resolve(products));
      const result = await service.getProducts(randomLimit, 0);
      console.log(result);

      expect(result.length).toEqual(randomLimit);
      expect(result).toMatchObject(products);
    });

    test("should get products throw products not exits", async () => {
      const service = new CatalogService(repository);
      jest
        .spyOn(repository, "find")
        .mockImplementationOnce(() =>
          Promise.reject(new Error("Products not exits"))
        );

      await expect(service.getProducts(0, 0)).rejects.toThrow(
        "Products not exits"
      );
    });
  });

  describe("getProduct", () => {
    test("should get product by id", async () => {
      const service = new CatalogService(repository);
      const product = productFactory.build();

      jest
        .spyOn(repository, "findOne")
        .mockImplementationOnce(() => Promise.resolve(product));

      const result = await service.getProduct(product.id!);

      expect(result).toMatchObject(product);
    });
  });

  describe("deleteProduct", () => {
    test("should delete product by id", async () => {
      const service = new CatalogService(repository);

      const product = productFactory.build();
      jest
        .spyOn(repository, "delete")
        .mockImplementationOnce(() => Promise.resolve({ id: product.id }));

      const result = await service.deleteProduct(product.id!);

      expect(result).toMatchObject({ id: product.id });
    });
  });
});
