import { IsString, IsNotEmpty, IsNumber, Min } from "class-validator";
export class CreateProductRequest {
  @IsString()
  @IsNotEmpty({
    message: "name is required",
  })
  name: string;

  @IsString()
  description?: string;

  @IsNumber()
  price?: number;

  @IsNumber()
  stock?: number;
}

export class UpdateProductRequest {
  @IsString()
  name?: string;

  @IsString()
  description?: string;

  @Min(1, {
    message: "Price min is 1",
  })
  @IsNumber()
  price?: number;

  @Min(0)
  @IsNumber()
  stock?: number;
}
