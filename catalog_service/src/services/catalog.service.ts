import { DtoInputProduct } from "./../interface/repository/catalog.interface";
import { ICatalogRepository } from "../interface/repository/catalog.interface";

export class CatalogService {
  private repository: ICatalogRepository;
  constructor(repository: ICatalogRepository) {
    this.repository = repository;
  }
  async createProduct(input: DtoInputProduct) {
    const data = await this.repository.create(input);
    console.log(data);
    if (!data.id) {
      throw new Error("unable to create product");
    }
    return data;
  }

  async updateProduct(input: DtoInputProduct) {
    const data = await this.repository.update(input);
    if (!data.id) {
      throw new Error("unable to  product");
    }
    return data;
  }

  async getProducts(limit: number, offset: number) {
    return await this.repository.find(limit, offset);
  }

  async getProduct(id: number | string) {
    return await this.repository.findOne(id);
  }

  async deleteProduct(id: number | string) {
    return await this.repository.delete(id);
  }
}
