import express, { NextFunction, Request, Response } from "express";
import { CatalogService } from "../services/catalog.service";
import { CatalogRepository } from "../repository/catalog.repository";
import { RequestValidator } from "../utils/requestValidator";
import {
  CreateProductRequest,
  UpdateProductRequest,
} from "../dtos/product.dto";

const catalogRouter = express.Router();

export const catalogService = new CatalogService(new CatalogRepository());

catalogRouter.post("/", (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.name)
    return res.status(400).json({
      message: "name is required",
    });
  return res.status(200).json({
    message: "Hello apis",
  });
});

catalogRouter.post(
  "/product",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { error, input } = await RequestValidator(
        CreateProductRequest,
        req.body
      );

      if (error) {
        return res.status(400).json(error);
      }

      const data = await catalogService.createProduct(req.body);
      return res.status(201).json({
        message: "Hello apis",
        data,
      });
    } catch (error) {
      const err = error as Error;
      return res.status(500).json(err.message);
    }
  }
);

catalogRouter.patch("/product/:id", async (req: Request, res: Response) => {
  try {
    const { error } = await RequestValidator(UpdateProductRequest, req.body);

    if (error) {
      console.log(error);
      return res.status(400).json(error);
    }
    const id = req.params.id;
    const data = await catalogService.updateProduct({ ...req.body, id });
    return res.status(200).json({
      message: "update success",
      data,
    });
  } catch (error) {
    const err = error as Error;
    return res.status(500).json(err.message);
  }
});

catalogRouter.get("/product", async (req: Request, res: Response) => {
  try {
    const limit = parseInt(req.query.limit as string) || 10;
    const offset = parseInt(req.query.offset as string) || 0;

    const data = await catalogService.getProducts(limit, offset);
    return res.status(200).json({
      message: "get products successfully",
      data,
    });
  } catch (error) {
    const err = error as Error;
    return res.status(500).json(err.message);
  }
});

catalogRouter.get("/product/:id", async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const productId = parseInt(id) || 0;

    const data = await catalogService.getProduct(productId);

    return res.status(200).json({
      message: "get product successfully",
      data,
    });
  } catch (error) {
    const err = error as Error;
    return res.status(500).json(err.message);
  }
});

catalogRouter.delete("/product/:id", async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const productId = parseInt(id) || 0;

    const data = await catalogService.deleteProduct(productId);

    return res.status(200).json({
      message: "delete product successfully",
      data,
    });
  } catch (error) {
    const err = error as Error;
    return res.status(500).json(err.message);
  }
});

export default catalogRouter;
