import { VendorService } from "./../services/vendor.service";
import { CreateVendorInput, VendorLoginInputs } from "./../dtos/vendor.dto";
import { Response, Request } from "express";
class VendorController {
  async create(req: Request, res: Response) {
    try {
      const input = <CreateVendorInput>req.body;

      const data = await VendorService.create(input);

      return res.status(201).json({
        message: "create vendor successfully",
        data,
      });
    } catch (error) {
      const err = error as Error;
      res.status(500).json({
        message: err.message,
      });
    }
  }

  async update(req: Request, res: Response) {
    return res.status(200).json({
      message: "update vendor successfully",
    });
  }

  async gets(req: Request, res: Response) {
    const data = await VendorService.getAll();
    return res.status(200).json({
      message: "get vendors successfully",
      data,
    });
  }

  async get(req: Request, res: Response) {
    const id = req.params.id;
    const data = await VendorService.getOne(id);
    return res.status(200).json({
      message: "get vendor successfully",
      data,
    });
  }

  async delete(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const success = await VendorService.deleteById(id);
      return res.status(200).json({
        message: "delete vendor successfully",
        success,
      });
    } catch (error) {
      const err = error as Error;
      return res.status(500).json({
        message: err.message,
      });
    }
  }

  async login(req: Request, res: Response) {
    try {
      const input = <VendorLoginInputs>req.body;
      const token = await VendorService.login(input.email, input.password);

      return res.status(200).cookie("access_token", token).json({
        message: "login successfully",
        token,
      });
    } catch (error) {
      const err = error as Error;
      return res.status(500).json({
        message: err.message,
      });
    }
  }
}

export const vendorController = new VendorController();
