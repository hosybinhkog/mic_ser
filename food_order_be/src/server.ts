import dotenv from "dotenv";
import express, { Request, Response } from "express";
import helmet from "helmet";
import cors from "cors";
import { createStream } from "rotating-file-stream";
import cookieParser from "cookie-parser";

import morgan = require("morgan");
import mainRoutes from "./routes";
import path = require("node:path");

dotenv.config();

const app = express();
const PORT = process.env.PORT || 3333;

const stream = createStream("file-access-routes.log", {
  size: "10M", // rotate every 10 MegaBytes written
  interval: "1d", // rotate daily
  compress: "gzip", // compress rotated files
  path: path.join(__dirname, "log"),
});

app.use(helmet());
app.use(morgan("combined", { stream }));
app.use(cors());
app.use(cookieParser());

app.use(express.json());
app.use(express.urlencoded({ extended: true, limit: "50mb" }));

import "./dbs/connect.db";

app.use("/hello", (req: Request, res: Response) => {
  return res.status(200).json({
    message: "hello",
  });
});

app.use("/api/dev/v1/", mainRoutes);

app.listen(PORT, () => {
  console.clear();
  console.log(`server running on http://localhost:${PORT}`);
});
