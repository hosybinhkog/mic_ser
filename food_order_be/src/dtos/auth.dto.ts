import { VandorPayload } from "./vendor.dto";

export type AuthPayload = VandorPayload;
