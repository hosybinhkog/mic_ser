import { AuthPayload } from "./../dtos/auth.dto";
import { VandorPayload } from "./../dtos/vendor.dto";
import jwt from "jsonwebtoken";

export const GenerateSignature = async (payload: VandorPayload) => {
  return await jwt.sign(payload, process.env.SECRET_TOKEN as string, {
    expiresIn: "2 days",
  });
};

export const ValidateSignature = async (token: string) => {
  const payload = (await jwt.verify(
    token,
    process.env.SECRET_TOKEN as string
  )) as AuthPayload;

  return payload;
};
