import { AuthPayload } from "./dtos/auth.dto";

declare namespace Express {
  export interface Request {
    user?: AuthPayload;
  }
}
