import { vendorController } from "./../controllers/vendor.controller";
import express from "express";

const vendorRoutes = express.Router();

vendorRoutes.get("/hello", (_req, res) => {
  return res.status(200).json({
    message: "hello vendor",
  });
});

vendorRoutes.post("/", vendorController.create);
vendorRoutes.patch("/:id", vendorController.update);
vendorRoutes.get("/", vendorController.gets);
vendorRoutes.post("/login", vendorController.login);
vendorRoutes.get("/:id", vendorController.get);
vendorRoutes.delete("/:id", vendorController.delete);

export default vendorRoutes;
