import express from "express";

const adminRoutes = express.Router();

adminRoutes.get("/hello", (_req, res) => {
  return res.status(200).json({
    message: "hello admin routes",
  });
});

export default adminRoutes;
