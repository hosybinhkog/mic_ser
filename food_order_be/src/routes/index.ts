import express from "express";
import adminRoutes from "./admin.routes";
import vendorRoutes from "./vendor.routes";

const mainRoutes = express.Router();

mainRoutes.use("/admin", adminRoutes);
mainRoutes.use("/vendor", vendorRoutes);

export default mainRoutes;
