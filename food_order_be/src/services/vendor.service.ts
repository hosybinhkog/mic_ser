import { GenerateSignature } from "./../utils/token.util";
import {
  GenerateSalt,
  GeneratePassword,
  ValidatePassword,
} from "./../utils/password.util";
import { Vendor } from "./../models";
import { CreateVendorInput } from "./../dtos/vendor.dto";

export class VendorService {
  static async create(input: CreateVendorInput) {
    const { email, password } = input;
    const existVendor = await this.findVendor(undefined, email);
    if (existVendor) {
      throw new Error("vendor is exist");
    }
    const salt = await GenerateSalt();
    const passwordHash = await GeneratePassword(password, salt);

    const vendor = await Vendor.create({
      ...input,
      rating: 0,
      salt,
      coverImages: [],
      serviceAvailable: false,
      password: passwordHash,
    });
    await vendor.save();

    return vendor;
  }

  static async getAll() {
    const vendors = await Vendor.find({});

    if (!vendors) throw new Error("Vendors data not available");

    return vendors;
  }

  static async getOne(id: string) {
    const vendor = await Vendor.findById(id);
    if (!vendor) throw new Error("product not found");
    return vendor;
  }

  static async deleteById(id: string) {
    const vendor = await Vendor.findByIdAndDelete(id);
    if (!vendor) throw new Error("product not found");

    return true;
  }

  static async findVendor(id: string | undefined, email?: string) {
    const vendor = email
      ? await Vendor.findOne({ email }).lean()
      : await Vendor.findById(id).lean();

    return vendor;
  }

  static async login(email: string, password: string) {
    const existVandor = await this.findVendor(undefined, email);
    if (!existVandor) throw new Error("Vendor not found");

    const math = await ValidatePassword(
      password,
      existVandor.password,
      existVandor.salt
    );
    if (!math) throw new Error("Wrong password or email");

    const token = await GenerateSignature({
      _id: existVandor._id,
      email,
      foodType: existVandor.foodType as unknown as string,
      name: existVandor.name,
    });

    return token;
  }

  static async getVandorProfile() {}

  static async updateVandorProfile() {}

  static async updateVandorService() {}
}
