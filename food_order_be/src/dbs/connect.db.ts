import mongoose from "mongoose";

class DatabaseMongo {
  private static instance: DatabaseMongo;

  constructor() {
    this.connect();
  }

  connect() {
    mongoose.set("debug", true);
    mongoose.set("debug", { color: true });

    mongoose
      .connect(process.env.MONGO_URI as string)
      .then(() => console.log("connect db"))
      .catch((err) => console.log(err.nessage));
  }

  static getInstance(): DatabaseMongo {
    if (!DatabaseMongo.instance) {
      DatabaseMongo.instance = new DatabaseMongo();
    }

    return DatabaseMongo.instance;
  }
}
const instanceMongodb: DatabaseMongo = DatabaseMongo.getInstance();
export default instanceMongodb;
